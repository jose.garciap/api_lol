const router = require('express-promise-router')();

const {
    index,
    newPersonaje,
    getUser
} = require('../controllers/personajes');

router.get('/', index);
router.post('/', newPersonaje);
router.get('/:_id', getUser);

module.exports = router;