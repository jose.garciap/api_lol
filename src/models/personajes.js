const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const personajesSchema = new Schema ({
    nombre: String,
    tipo: String,
}); 

module.exports = mongoose.model('Personaje', personajesSchema);