const Personaje = require('../models/personajes');


module.exports = {

    index: async(req, res, next) => {
        const personajes = await Personaje.find({});
        res.status(200).json(personajes);
    },

    newPersonaje: async (req, res, next) => {
        const newPersonaje = new Personaje(req.body);
        const personaje = await newPersonaje.save();
        res.status(200).json(personaje);
    },

    getUser: async (req, res, next) => {
        const {_id} = req.params;
        const personaje = await Personaje.findById(_id);
        res.status(200).json(personaje);
    }

     


};